## JettPack

JettPack is a fork of [Pufferfish](https://github.com/pufferfish-gg/Pufferfish) with experimental patches. This fork is not recommended for use in production, although I'm aiming to be able to consider the main branch stable soon(tm).

### Here's some of the most notable additions my fork contains:
1. Reduced allocations, reducing memory usage and therefore the frequency at which garbage collection runs.
2. [Alternate Current](https://github.com/SpaceWalkerRS/alternate-current) Redstone algorithm (Substantially faster than Eigencraft or Vanilla's Redstone algorithm).
3. Tons of new optimizations from [Lithium](https://github.com/CaffeineMC/lithium-fabric). Resulting is much less CPU usage while still being vanilla-friendly.
4. NBT Cache which reduces I/O operations for fetching Player Data.
5. Faster random implementation.
6. Reduction in unnecessary packets (thanks to [Slice](https://github.com/Cryptite/Slice)).
7. More efficient handling of async tasks.
8. [Hydrogen](https://github.com/CaffeineMC/hydrogen-fabric) for lower memory usage.
9. Math optimizations in relation to chunk generation from [C2ME](https://github.com/RelativityMC/C2ME-fabric).
10. And much, much more! Checkout our [patches](https://gitlab.com/Titaniumtown/JettPack/-/tree/main/patches/server) for a full list of modifications.

Jenkins (for downloads): https://www.gardling.com/jenkins

Discord: https://discord.gg/U9eQ93d2pB